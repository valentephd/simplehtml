<?php
/**
 * Created by PhpStorm.
 * User: u8557373
 * Date: 18/07/2018
 * Time: 16:43
 */

$string['pluginname'] = 'Bloco Simples HTML';
$string['simplehtml'] = 'Título do Bloco';
$string['simplehtml:addinstance'] = 'Adicionar um novo Bloco Simples HTML';
$string['simplehtml:myaddinstance'] = 'Adicionar um novo Bloco Simples HTML na página do Moodle';
$string['blockstring'] = 'Informe o conteúdo do bloco: ';
$string['defaulttitle'] = 'Título do Bloco';
