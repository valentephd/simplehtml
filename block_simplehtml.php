<?php
/**
 * Created by PhpStorm.
 * User: u8557373
 * Date: 18/07/2018
 * Time: 16:40
 */

class block_simplehtml extends block_base {
    public function init() {
        $this->title = get_string('simplehtml', 'block_simplehtml');
    }

    public function get_content() {
        if ($this->content !== null) {
            return $this->content;
        }

        $this->content         =  new stdClass;
        if (! empty($this->config->text)) {
            $this->content->text = $this->config->text;
        }
        else {
            $this->content->text   = 'Seu conteúdo aqui.';
        }
        $this->content->footer = 'Rodapé ...';

        return $this->content;
    }

    public function specialization() {
        if (isset($this->config)) {
            if (empty($this->config->title)) {
                $this->title = get_string('defaulttitle', 'block_simplehtml');
            } else {
                $this->title = $this->config->title;
            }

            if (empty($this->config->text)) {
                $this->config->text = get_string('defaulttext', 'block_simplehtml');
            }
        }
    }
}